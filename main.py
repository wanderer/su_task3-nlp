#!/usr/bin/env python3
"""
this file is part of the da_detector NLP project
"""


import da_detector as detector


test_str = "what freaking ever, nobody cares one bit of a heck"
test_str_sk = "Nad Tatrou sa blýska, hromy divo bijú."
test_str_sk_long = """1. Nad Tatrou sa blýska
hromy divo bijú,
nad Tatrou sa blýska
hromy divo bijú,
Zastavme ich, bratia,
veď sa ony stratia,
Slováci ožijú.
2. To Slovensko naše
posiaľ tvrdo spalo,
ale blesky hromu
vzbudzujú ho k tomu,
aby sa prebralo.
3. Ešte jedle rastú
na krivánskej strane
Kto jak Slovák cíti,
nech sa šable chytí,
a medzi nás stane.
4. Už Slovensko vstáva,
putá si strháva
Hej rodina milá
hodina odbila,
žije matka Sláva!"""

all_langs = ["sk", "cz", "de", "en", "fr"]


d = detector.da_detector()

d.what_this(test_str, ["sk", "de", "en"])
d.what_this(test_str, [])
d.what_this(test_str_sk, [])
d.what_this(test_str_sk_long, all_langs)

# vim: ff=unix noexpandtab
